//create a canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

//Background image
var bgImage = new Image();
bgImage.src = "images/background.png"

//Hero image
var heroImage = new Image();
heroImage.src = "images/hero.png"

//Monster image
var monsterImage = new Image();
monsterImage.src = "images/monster.png"

//Game objects
var hero = {speed: 1};
var monster = {};
var monstersCaught = 0;

function reset()
{
    //hero position
    hero.x = canvas.width/2;
    hero.y = canvas.height/2;
    
    //monster's position
    monster.x = 32 + (Math.random() * (canvas.width-64));
    monster.y = 32 + (Math.random() * (canvas.height-64));
}

//Handle keyboard control
var keysDown = new Set();

document.addEventListener("keydown", function(e) {keysDown.add(e.keyCode)} );
document.addEventListener("keyup", function(e) { keysDown.delete(e.keyCode)} );

//Update game objects
function update()
{
    //Player holding up
    if(keysDown.has(38))  { hero.y -= hero.speed; }
    //Player holding down
    if (keysDown.has(40)) {hero.y += hero.speed; }
    //Player holding left
    if (keysDown.has(37)) {hero.x -= hero.speed;}
    //player holding right
    if (keysDown.has(39)) {hero.x += hero.speed;}
    
    //Are they touching?
    if ((hero.x <= monster.x + 32) && (monster.x <= hero.x + 32) 
    && (hero.y <= monster.y + 32) && (monster.y <= hero.y + 32))
        {
            monstersCaught++;
            reset();
        }
    
}

//draws the game 
function render()
{
    ctx.drawImage(bgImage, 0, 0);
    ctx.drawImage(heroImage, hero.x, hero.y);
    ctx.drawImage(monsterImage, monster.x, monster.y);
    
    //draw text
    ctx.fillStyle = "rgb(250, 250, 250)"; 
    ctx.font = "24px Helvetica";
    ctx.textAlign = "left";
    ctx.textBaseline = "top";
    ctx.fillText("Goblins Caught: " + monstersCaught, 32, 32);
}

//the main game loop
function main()
{
    update();
    render();
}

reset();
//Let's play this game!
window.setInterval(main,1); //execute as fast as possible